import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsociadosComponent } from './components/asociados/asociados.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { ComunicadosComponent } from './components/comunicados/comunicados.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { EditorialesComponent } from './components/editoriales/editoriales.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { LibrosVendedorComponent } from './components/libros-vendedor/libros-vendedor.component';
import { LibrosComponent } from './components/libros/libros.component';
import { LoginComponent } from './components/login/login.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { UnLibroComponent } from './components/un-libro/un-libro.component';

const routes: Routes = [
  {path: 'nosotros', component: NosotrosComponent},
  {path: 'index', component: InicioComponent},
  {path: 'registrar', component: RegistrarComponent},
  {path: 'login', component: LoginComponent},
  {path: 'editoriales', component: EditorialesComponent},
  {path: 'eventos', component: EventosComponent},
  {path: 'comunicados', component: ComunicadosComponent},
  {path: 'asociados', component: AsociadosComponent},
  {path: 'contacto', component: ContactoComponent},
  {path: 'libros', component: LibrosComponent},
  {path: 'libroVendedor/:id', component: LibrosVendedorComponent},
  {path: 'Un-libro/:id', component: UnLibroComponent},
  {path: 'buscar/:termino', component: BuscadorComponent}, //añadimos un nuevo componente

  { path: 'admin',
  loadChildren: () => import('./components/admin/admin.module').then(x => x.AdminModule)
},

  { path: 'dashboard',
  loadChildren: () => import('./components/dashboard/dashboard.module').then(x => x.DashboardModule)
},

  {path: '**', pathMatch: 'full', redirectTo: 'index'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
