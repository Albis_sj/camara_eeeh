import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VendedorService } from 'src/app/servicios/vendedor.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  mostrar = false;
  userName!: number;

  alerta: string = 'alerta exitosa';
  propiedades: any = {
    visible: false
  }

  constructor( private _vendedor: VendedorService,
               private router: Router) {
    let nombre = sessionStorage.getItem('ID')
if (nombre === null){
  this.userName = 0
} else {
  this.userName = parseInt(nombre)
  this.mostrar = true
}

   }

  ngOnInit(): void {
  }

  irAlPerfil(){
    this.router.navigate([ '/dashboard/perfil', this.userName ])
  }

}
