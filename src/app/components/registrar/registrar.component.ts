import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { VendedorService } from 'src/app/servicios/vendedor.service';


interface HtmlInputEvent extends Event{
  target: HTMLInputElement & EventTarget;
}



@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  file!: File;
  photoSelected!: string | ArrayBuffer | null;

  enviado: string = 'Registro finalizado con éxito';
  mensaje!: string;

  propiedades: any = {
    error: false
  }

  categoria: string[]=['Editorial', 'Distribuidora', 'Autor']
  formulario!: FormGroup;

  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef
  apiUrl = 'http://localhost:3000/vendedor';
  url = 'https://i.pinimg.com/736x/b5/49/41/b5494197b2d462c940f88988b203d290.jpg'

  constructor(private fb: FormBuilder,
              private _vendedorSVC : VendedorService,
              private router : Router,
              private http: HttpClient
              ) { 

    this.crearFormulario();
  }


  // VALIDACIONES
    get nombreNoValido(){
      return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched
    }

    get categoriaNoValido(){
      return this.formulario.get('categoria')?.invalid && this.formulario.get('categoria')?.touched
    }

    get correo1NoValido(){
      return this.formulario.get('correo1')?.invalid && this.formulario.get('correo1')?.touched
    }

    get contraseniaNoValido(){
      return this.formulario.get('contrasenia')?.invalid && this.formulario.get('contrasenia')?.touched
    }
    
    get telefono1NoValido(){
      return this.formulario.get('telefono1')?.invalid
    }
    
    get telefono2NoValido(){
      return this.formulario.get('telefono2')?.invalid
    }
        
    get celular1NoValido(){
      return this.formulario.get('celular1')?.invalid
    }
        
    get celular2NoValido(){
      return this.formulario.get('celular2')?.invalid 
    }
    
    get correo2NoValido(){
      return this.formulario.get('correo2')?.invalid 
    }
    
    get paginaNoValido(){
      return this.formulario.get('pagina')?.invalid
    }
    
  crearFormulario(): void {
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      categoria: ['', Validators.required],
      correo1: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      contrasenia: ['', [Validators.required, Validators.minLength(4)]],
      telefono1: ['', Validators.minLength(5)],
      telefono2: ['', Validators.minLength(5)],
      celular1: ['', Validators.minLength(8)],
      celular2: ['', Validators.minLength(8)],
      correo2: ['', Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)],
      direccion: ['', Validators.minLength(10)],
      pagina: ['', Validators.minLength(5)]
    })
  }

  readData:any;



  ngOnInit(): void {
  }

  guardar(){
    // console.log(this.formulario.value);


    // this.onFileUpload(this.formulario.value.nombre);
    if(this.formulario.valid){

      console.log(this.formulario.value);
      this._vendedorSVC.createData(this.formulario.value).subscribe((res)=>{
        console.log(res, 'res create REgistro==>');

      // this.getAllData();
        
      });

      this.mensaje = this.enviado;
      setTimeout(() => {
        this.mensaje = '';
      }, 4000);

    this.propiedades.error = true;
    setTimeout(() => {
      this.propiedades.error =false;
    }, 4000);
    this.router.navigate(['/login'])
  }

  }

  // On File Select
  onSelect(event: any) {
    this.file = event.target.files[0]

    if(event.target.files[0]){
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result
      }
    }
  }


}






export interface Photo{
  _id?: string;
  title: string;
  descripcion: string;
  imagePath: string;
}