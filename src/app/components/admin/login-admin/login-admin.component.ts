import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { VendedorLoginI } from 'src/app/interfaces/vendedor.interface';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent implements OnInit {

  formulario!: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router) {
    this.crearFormulario();
   }

  crearFormulario(): void {

    this.formulario = this.fb.group({
      correo1: ['', [Validators.required]],
      contrasenia: ['', [Validators.required, Validators.minLength(4)]],
      // recuerdame: ['']
    });
  }

  ingresar(){

    console.log(this.formulario.value);
    const Vendedor: VendedorLoginI = {
      correo1: this.formulario.value.correo1,
      contrasenia: this.formulario.value.contrasenia
    }

    if(Vendedor.correo1=== 'admin' && Vendedor.contrasenia=== '20libro20'){
      this.router.navigate([ '/admin/addEvent' ])
      console.log('bien');
      
    } else {
      this.router.navigate([ '/index' ])
    }

  }


  ngOnInit(): void {
  }

}
