import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComunicadosComponent } from './comunicados/comunicados.component';
import { EventosComponent } from './eventos/eventos.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';

const routes: Routes = [
  { path: 'addEvent', component: EventosComponent},
  { path: 'addComunicado', component: ComunicadosComponent},
  { path: 'Admin-login', component: LoginAdminComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'Admin-login'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }