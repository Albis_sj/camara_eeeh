import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { EaddEventosService } from 'src/app/servicios/eadd-eventos.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {

    
  propiedades: any = {
    error: false
  }
  tipo_archivo: any;


  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef
  formulario!:FormGroup;
  formOneData!: FormI;
  file!: File;
  apiUrlImg = 'http://localhost:3000/addEventImg';
  url = 'https://i.pinimg.com/736x/b5/49/41/b5494197b2d462c940f88988b203d290.jpg';
  readData!: FormI[];
  rutaImg='../../../../assets/img/eventos/'


  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private addEventSVC: EaddEventosService) {

    this.crearFormulario();
    this.getInfo();
   }

   crearFormulario(){
    this.formulario = this.fb.group({
      titulo: ['', Validators.required],
      descripcion: ['', Validators.minLength(7)],
      imagenes: ['', Validators.required]
    })
   }

    ngOnInit(): void {
    }

    guardar(){
      console.log(this.formulario.value, 'PRIMERO');

      let nombreImagen = this.formulario.value.imagenes

        const imgName = nombreImagen.split('\\');
        console.log(imgName);
        const dividir = imgName[2].split('.');
        console.log(dividir);

        const extension = this.tipo_archivo.split('/');

        
      
        this.formOneData = {
          id_addEvent: 1,
          titulo: this.formulario.value.titulo,
          descripcion: this.formulario.value.descripcion,
          imagen1: `../../../../assets/img/eventos/${dividir[0]}.${dividir[1]}`,
          tipo1: `.${extension[1]}`
        }

        console.log('SEGUNDO', this.formOneData, 'SEGUNDO');
        
        this.onFileUpload()
        this.createData(this.formOneData);
    }

    // On File Select
    onSelect(event: any) {
      this.file = event.target.files[0]
      console.log(this.file.type);
      const filew = event.target.files[0]
      this.tipo_archivo = filew.type

      if(event.target.files[0]){
        let reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (event: any) => {
          this.url = event.target.result
        }
      }
    }

    onFileUpload(){
      console.log('upload foto TERVERO');
      
      const imageBlob = this.fileInput.nativeElement.files[0];
      const file = new FormData();
      console.log('TERCERO', file, 'TERCERP');
      
  
      file.set('file', imageBlob);
  
      this.http.post(`${this.apiUrlImg}`, file).subscribe(res => {
        // console.log(res);
        console.log('CUARTO', file, 'CUARTO');
        
      });
    }



    // BASE DE DATOS
    // get all data
    getInfo(){
      this.addEventSVC.getAllData().subscribe((res)=>{
        console.log(res, "res==>");
        this.readData = res.data;
      });
    }
    
    //create data
    createData(data:any,){
      console.log(data);
      this.addEventSVC.createData(data).subscribe((res)=>{
        console.log(res, 'res create REgistro==>');
        this.getInfo();
      });
    }

  //delete data
  deleteData(id: any){
    console.log(id, 'deleteid==>');
    this.addEventSVC.deleteData(id).subscribe((res)=>{
      console.log(res, 'deleteRes ==>');
      this.getInfo();
    });
  }
}


export interface FormI{ 
  id_addEvent: number
  titulo: string,
  descripcion: string,
    imagen1: string,
    tipo1: string,
    imagen2?: string,
    tipo2?: string,
    imagen3?: string,
    tipo3?: string,
    imagen4?: string,
    tipo4?: string,
    imagen5?: string,
    tipo5?: string,
    imagen6?: string,
    tipo6?: string,
    imagen7?: string,
    tipo7?: string,
    imagen8?: string,
    tipo8?: string,
    imagen9?: string,
    tipo9?: string,
    imagen10?: string,
    tipo10?: string,
}