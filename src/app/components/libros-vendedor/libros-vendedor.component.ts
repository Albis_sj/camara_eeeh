import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VendedorI } from 'src/app/interfaces/vendedor.interface';
import { LibroService } from 'src/app/servicios/libro.service';
import { VendedorService } from 'src/app/servicios/vendedor.service';

@Component({
  selector: 'app-libros-vendedor',
  templateUrl: './libros-vendedor.component.html',
  styleUrls: ['./libros-vendedor.component.css']
})
export class LibrosVendedorComponent implements OnInit {

  libros: any[] = [];
  termino!: number;
  readCategoria:any = []
  mensaje:any;
  mostrar: boolean = true;
  vendedor: VendedorI = {
    nombre: '',
    id_vendedor: 0,
    categoria: '',
    correo1: '',
    correo2: 0,
    contrasenia: '',
    telefono1: '',
    telefono2: '',
    celular1: 0,
    celular2: '',
    direccion: '',
    pagina: ''
    };

  constructor(private _librosSVC: LibroService,
    private router: Router,
    private _vendedorSVC: VendedorService,
    private activatedRoute: ActivatedRoute) { 

    this.getAllData()
    this.activatedRoute.params.subscribe(params => { //subscribe obtiene una variable
      this.termino = parseInt(params['id']); //se lo guarda en una variables lo que hace log, y esa variable 
    
      //NO PASA DE AQUI

      this._vendedorSVC.getOneData(this.termino).subscribe((res)=>{
        console.log(res, "res==> libros Filtrados");
        // this.readCategoria = res.data;
        // this.mensaje = res.message
        this.vendedor = res.data[0]
        console.log(this.vendedor, 'ooo');
      });
      

      this._librosSVC.getLibroEdi(this.termino).subscribe((res)=>{
        console.log(res, "res==> libros Filtrados");
        this.readCategoria = res.data;
        this.mensaje = res.message
        console.log(this.mensaje, '000');
        // if(this.readCategoria = undefined){
        //   this.mostrar = true
        // } 
        
      });

      console.log(this.readData);
      

      // this.libros = this._librosSVC.getLibroEdi(this.termino);

      // console.log(this.libros, 'lista de libros');
    })

  }

  ngOnInit(): void {
  }


  //jhgjkhgjhghj
  readData: any;
  //get Data
  getAllData() {
    console.log('get data');

    this._librosSVC.getAllData().subscribe((res) => {
      console.log(res, "res==>");
      this.readData = res.data;
    });}

}
