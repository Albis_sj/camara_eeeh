import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibrosVendedorComponent } from './libros-vendedor.component';

describe('LibrosVendedorComponent', () => {
  let component: LibrosVendedorComponent;
  let fixture: ComponentFixture<LibrosVendedorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibrosVendedorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LibrosVendedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
