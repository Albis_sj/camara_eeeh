import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriasService } from 'src/app/servicios/categorias.service';
import { LibroService } from 'src/app/servicios/libro.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {

  formato:any;

  public pokemons: Pokemon[] = [];
  public page: number = 0;
  public number: number = 1;
  pageActual:number = 1;

  propiedades: any = {
    error: true
  }

  constructor(private libroSVC: LibroService,
              private categoriaSVC: CategoriasService,
              private router: Router) {
    
    this.Categoria();
    this.Libros();
    this.Formato();
  }

  ngOnInit(): void {

    this.libroSVC.getAllPolemons()
      .subscribe( pokemons => {
        this.pokemons = pokemons
      })
  }

  readCategoria: any;
  readFormato: any;

  Categoria(){
    this.categoriaSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==> categoria");
      this.readCategoria = res.data;
    });
  }

  Formato(){
    this.formato = this.libroSVC.getFormato()
  }

  Libros(){
    this.libroSVC.getAllData().subscribe((res)=>{
      console.log(res, "res==> libros");
      this.readCategoria = res.data;
    });
  }

  buscarLibro(termino: string): void{
    this.router.navigate(['/buscar', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
  }



  nextPage(){
    this.page += 5
    this.number += 1
    // if(this.number === 0){ this.text = this.textErr}
    // console.log(this.number);
    
  }
  prevPage(){
    if(this.page > 0)
    this.page -= 5
    this.number -= 1
    if (this.number === 0){
      // this.text = this.textErr
    }
    console.log(this.number);
  }


  VerLibro(id:number){
    this.router.navigate(['/Un-libro', id]);
  }

}




export interface FetchAllPokemonResponse {
  conunt: number;
  next: null;
  previous: null;
  results: SmallPokemon[];
}

export interface SmallPokemon {
  name: string;
  url: string;
}

export interface Pokemon {
  id: string;
  name: string;
  pic: string;
}