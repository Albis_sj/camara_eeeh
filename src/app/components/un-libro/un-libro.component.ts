import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VendedorI } from 'src/app/interfaces/vendedor.interface';
import { LibroService } from 'src/app/servicios/libro.service';
import { VendedorService } from 'src/app/servicios/vendedor.service';

@Component({
  selector: 'app-un-libro',
  templateUrl: './un-libro.component.html',
  styleUrls: ['./un-libro.component.css']
})
export class UnLibroComponent implements OnInit {

  id_book!: number
  libro!: {};

  readData:any;
  vendedor!: VendedorI;
  constructor(private activatedRoute: ActivatedRoute,
              private _libroSVC: LibroService,
              private _VendedorSVC: VendedorService,
              private router: Router) {

    this.activatedRoute.params.subscribe(params => { //subscribe obtiene una variable
      this.id_book = parseInt(params['id']); //se lo guarda en una variables lo que hace log, y esa variable 
    
      console.log(this.id_book);
      
      //NO PASA DE AQUI
      this._libroSVC.getUnLibro(this.id_book).subscribe((res)=>{
        console.log(res, "res==> libros Filtrados");
        this.readData = res.data[0];
        console.log(this.readData);

        this.getVendedor(this.readData.id_vendedor)
        
        // this.readCategoria = res.data;
        // this.mensaje = res.message
        // this.libro = res.data[0]
        // console.log(this.libro, 'ooo');
      });
      

      // console.log(this.libros, 'lista de libros');
    })

   }

  ngOnInit(): void {
  }

  buscarLibro(termino: string): void{
    this.router.navigate(['/buscar', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
  }


  getVendedor(id_vendedor: number){
    this._VendedorSVC.getOneData(id_vendedor).subscribe((res) => {
      this.vendedor= res.data[0]
      console.log(this.vendedor);

    })
  }


}
