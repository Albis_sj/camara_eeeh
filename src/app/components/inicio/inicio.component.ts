import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LibroService } from 'src/app/servicios/libro.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {




  constructor(private activatedRoute: ActivatedRoute,
              private _librosSVC: LibroService,
              private router: Router) {
   }

  ngOnInit(): void {

  }

  readData:any;



  buscarLibro(termino: string): void{
    this.router.navigate(['/buscar', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
  }

}
