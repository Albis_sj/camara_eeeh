import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { comunicadoI } from '../admin/comunicados/comunicados.component';



interface Item {
  imageSrc: string;
  imageAlt: string
}


@Component({
  selector: 'app-comunicados',
  templateUrl: './comunicados.component.html',
  styleUrls: ['./comunicados.component.css']
})
export class ComunicadosComponent implements OnInit {

  
  propiedades: any = {
    error: true
  }
  @Input() galleryData: Item[] = []
  // @ViewChild('asImage') image!: ElementRef;
  
  apiUrl = 'http://localhost:3000/addcomunicado';
  readData!: comunicadoI[];

  constructor(private http: HttpClient) { 
    this.getInfo();
  }


  ngOnInit(): void {
  }
  
  Visualizar(){
  }

      // BASE DE DATOS
    
  // get all data
  getAllData(): Observable<any>{
    return this.http.get(`${this.apiUrl}`);
  }

  getInfo(){
    this.getAllData().subscribe((res) => {
      this.readData = res.data;
      console.log('sda', this.readData, 'adva');
    })
  }


}
