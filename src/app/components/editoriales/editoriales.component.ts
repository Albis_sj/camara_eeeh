import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VendedorI } from 'src/app/interfaces/vendedor.interface';
import { VendedorService } from 'src/app/servicios/vendedor.service';

@Component({
  selector: 'app-editoriales',
  templateUrl: './editoriales.component.html',
  styleUrls: ['./editoriales.component.css']
})
export class EditorialesComponent implements OnInit {

  ListaConFiltro:VendedorI[] = [];

  constructor(private _vendedorSVC : VendedorService,
              private router: Router) {

    this.getAll();
   }

  readData:any;

  ngOnInit(): void {
  }

  getAll(){
    this._vendedorSVC.getAllData().subscribe((res)=>{
      
      console.log(res, "res==> Vendedor");
      this.readData = res.data;    

      for (let i = 0; i < this.readData.length; i++){
        if(this.readData[i].categoria === 'Editorial'){

          this.ListaConFiltro.push(this.readData[i])
          console.log('lista de editoriales' + this.ListaConFiltro);
        }
      }
    });
  }


  
  VerLibroEdi(id_vendedor: number){
    this.router.navigate(['/libroVendedor', id_vendedor]); 
  }


  buscarLibro(termino: string): void{
    this.router.navigate(['/buscar', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
  }


}
