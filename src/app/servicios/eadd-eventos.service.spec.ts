import { TestBed } from '@angular/core/testing';

import { EaddEventosService } from './eadd-eventos.service';

describe('EaddEventosService', () => {
  let service: EaddEventosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EaddEventosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
