import { HttpClient } from '@angular/common/http';
import { ElementRef, Injectable } from '@angular/core';
import { map, Observable, retry } from 'rxjs';
import { FetchAllPokemonResponse, Pokemon } from '../components/libros/libros.component';
import { LibroI } from '../interfaces/libro.interface';


@Injectable({
  providedIn: 'root'
})
export class LibroService {

  formato = [
    { id_formato: 1, formato: 'Libro de bolsillo'},
    { id_formato: 2, formato: 'De tapa dura'},
    { id_formato: 3, formato: 'Letra grande'},
    { id_formato: 4, formato: 'Audiolibro audible'},
    { id_formato: 5, formato: 'Código de acceso impreso'},
    { id_formato: 6, formato: 'Hoja suelta'},
    { id_formato: 7, formato: 'CD de audio'},
    { id_formato: 8, formato: 'Libro de cartón'},
    { id_formato: 9, formato: 'Encuadernado en espiral'},
  ]

  file!: File;
  libro!: LibroI;
  ListaLibro: LibroI[] = [];


  ListaConFiltro:LibroI[] = [];
  tabla: boolean = true;
  userName!: number;

  private url: string = 'https://pokeapi.co/api/v2'

  constructor(private _http: HttpClient) { 

    // this.getLibros();

    this.getAllD();
  }

  apiUrl = 'http://localhost:3000/libros';
  apiUrl_Un = 'http://localhost:3000/un-libro';
  apiUrlBuscar = 'http://localhost:3000/libros_buscador';
  Url = 'http://localhost:3000/libroEdi';

  getAllData(): Observable<any>{
    return this._http.get(`${this.apiUrl}`);
  }

  //get one data
  getOneData(id: number): Observable<any>{
    console.log('ser', id);

    return this._http.get(`${this.apiUrl}/${id}`);
  }

    //get one data
    getUnLibro(id: number): Observable<any>{
      console.log('ser', id);
  
      return this._http.get(`${this.apiUrl_Un}/${id}`);
    }
  


  getAll(userName: number){
    // console.log(userName);
    
    this.getAllData().subscribe((res)=>{
      
      this.readData = res.data;
      // console.log(this.readData, 'zzzzzzzz');
      
      for (let i = 0; i < this.readData.length; i++){
        if(this.readData[i].id_vendedor === userName){
          // console.log('bucle',this.readData[i]);
          // this.tabla= true;
          this.ListaConFiltro.push(this.readData[i])
          console.log( this.ListaConFiltro, 'libros del usuario');
          this.ListaLibro = this.ListaConFiltro;
        }
        if(this.ListaConFiltro.length === 0){
          this.tabla = false
        }
      }
      // console.log(this.ListaLibro);
    });
    
    this.getlibro();
    return this.ListaLibro;
  }

  getlibro(){
    console.log(this.ListaConFiltro);
    return this.ListaLibro
  }
  

  createData(data:any):Observable<any>{
    // console.log(data, 'create Api Libros =>');
    return this._http.post(`${this.apiUrl}`, data);
  }

  
  //delete data
  deleteData(id:any):Observable<any>{
    let ids = id;
    console.log(id, ids, 'service');
    return this._http.delete(`${this.apiUrl}/${ids}`);
  }


  getFormato(){
    return this.formato
  }

  readData: any;
  
  getAllD(){
    this.getAllData().subscribe((res)=>{
      this.readData = res.data;    
    });
  }
  
  //get Data
  getLibros(){
    this.getAllData().subscribe((res)=>{
      this.readData = res.data;
      this.ListaLibro = res.data;
    });
  }


    //get one data
    getBuscar(termino: string): Observable<any>{
      // console.log('ser');
      return this._http.get(`${this.apiUrlBuscar}/${termino}`);
    }

    
  buscarLibro(termino: string){
    console.log(termino, 'librosvc');
    
    let librosArr: LibroI[] = [];
    
    this.getLibros()

    for(let i = 0; i < this.ListaLibro.length; i++){


      let libro: LibroI = this.ListaLibro[i];
      console.log(libro);
      
      let nombre = libro.titulo.toLowerCase();
      console.log(nombre);

      if(nombre.indexOf(termino) >= 0){
        
        librosArr.push(libro);
        console.log(librosArr, 'libros array');
      }
    }

    return librosArr
  }


  // SUBIR LIBRO
  onSelect(event: any) {
    this.file = event.target.files[0]

    if(event.target.files[0]){
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.apiUrl = event.target.result
      }
    }
  }
  
  onFileUpload(fileInput : ElementRef){

    const imageBlob = fileInput.nativeElement.files[0];
    const file = new FormData();

    file.set('file', imageBlob);

    this._http.post(`${this.apiUrl}`, file).subscribe(res => {
      console.log(res);
    });
  }



  getAllPolemons(): Observable<Pokemon[]> {
    return this._http.get<FetchAllPokemonResponse>(`${this.url}/pokemon?limit=1500`)
              .pipe(
                map( this.transfomSmallPokemonIntoPokemon )
              )
  }

  private transfomSmallPokemonIntoPokemon( resp :  FetchAllPokemonResponse): Pokemon[] {

    const pokemonList: Pokemon[] = resp.results.map( poke => {


      const urlArr = poke.url.split('/')
      // console.log(urlArr);
      const id = urlArr[6];
      const pic = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`
      
      return {
        id,
        pic,
        name: poke.name,
      }
    })

    return pokemonList;
  }





  getLibroEdi(termino: any): Observable<any>{
    console.log(termino);
    return this._http.get(`${this.Url}/${termino}`);
  }

}
