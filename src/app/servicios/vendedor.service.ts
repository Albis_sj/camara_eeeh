import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { VendedorI, VendedorLoginI } from '../interfaces/vendedor.interface';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class VendedorService {

  user!: VendedorI;
  ID_user!: string;

  constructor(private _http: HttpClient,
              private router: Router) { }

  apiUrl = 'http://localhost:3000/vendedor';
  Vendedor!: VendedorLoginI;


  //get all data
  getAllData(): Observable<any>{
    return this._http.get(`${this.apiUrl}`);
  }


    //get all data
    getOneData(id: number): Observable<any>{
      // console.log('ser');
      return this._http.get(`${this.apiUrl}/${id}`);
    }

  //create data
  createData(data:any,):Observable<any>{
    console.log(data, 'create Api Vendedor =>');

    return this._http.post(`${this.apiUrl}`, data);
  }



    //LOGIN DATOS
    loginVendedor(user: VendedorI, id:number){
     this.ID_user = id.toString()
      this.user = user;
      sessionStorage.setItem('ID', this.ID_user);
      let nombre = sessionStorage.getItem('ID');
      
      this.router.navigate([ '/dashboard/perfil', id ])
      return  nombre;
    }

}
