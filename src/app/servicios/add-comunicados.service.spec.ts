import { TestBed } from '@angular/core/testing';

import { AddComunicadosService } from './add-comunicados.service';

describe('AddComunicadosService', () => {
  let service: AddComunicadosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddComunicadosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
