import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { EditorialesComponent } from './components/editoriales/editoriales.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { AsociadosComponent } from './components/asociados/asociados.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { LibrosComponent } from './components/libros/libros.component';
import { FiltroPipe } from './components/pipes/filtro.pipe';

import { NgxPaginationModule } from 'ngx-pagination';
import { LibrosVendedorComponent } from './components/libros-vendedor/libros-vendedor.component';
import { ComunicadosComponent } from './components/comunicados/comunicados.component';
import { UnLibroComponent } from './components/un-libro/un-libro.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    InicioComponent,
    NosotrosComponent,
    RegistrarComponent,
    LoginComponent,
    EditorialesComponent,
    EventosComponent,
    AsociadosComponent,
    ContactoComponent,
    BuscadorComponent,
    LibrosComponent,
    FiltroPipe,
    LibrosVendedorComponent,
    ComunicadosComponent,
    UnLibroComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
